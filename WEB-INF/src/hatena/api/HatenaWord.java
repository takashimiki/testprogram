package hatena.api;


import hatena.db.TitleSelect;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;



//はてなキーワード連想語API
public class HatenaWord{
	 private static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);


	 private static final String API_URL = "http://d.hatena.ne.jp/xmlrpc";
	 private static final String API_METHOD_NAME = "hatena.getSimilarWord";

	 //ワードセット
	 //private static final String[] Word_List = {"のんのんびより", "アニメ"};
	 //private static final String Word_List = "進撃の巨人";
	 //手動入力(笑)
	 /*static Scanner sc = new Scanner(System.in);
	 private static final String Word_List = sc.nextLine();
	 */

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) {
		List<String> Word_List = null;
		String str = "";
		try {
			//DB 呼び出し
			TitleSelect tSelect = new TitleSelect();
			ArrayList<String> title = new ArrayList<String>();
			title = (ArrayList<String>) tSelect.select();
//			List<String> title = tSelect.select();
			title.add(str);

			for(String list : title){
				Word_List = title;
				logger.info(list);
			}

			XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();

			config.setServerURL(new URL(API_URL));

			XmlRpcClient client = new XmlRpcClient();
			client.setConfig(config);

			//パラメータ
			Map param = new HashMap();
			param.put("wordlist", Word_List);


			//APIを呼んで結果を出力
			Object result;
			result = client.execute(API_METHOD_NAME, new Object[] {param});
			if(result != null && result instanceof Map){
				Object[] wordlist =(Object[]) ((Map) result).get("wordlist");
				for(Object word : wordlist){
					//System.out.println(Word_List);
					System.out.println(((Map) word).get("word"));
				}
			}
		} catch (SQLException e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (XmlRpcException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}
}
