package hatena.api;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

//はてなキーワード自動リンクAPI
public class Question {
	@SuppressWarnings({ "unchecked", "resource" })
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String text=sc.nextLine();
		
		//url指定
		XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
		try {
			config.setServerURL(new URL("http://d.hatena.ne.jp/xmlrpc"));
		} catch (MalformedURLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		
		//xmlrpcclient生成
		XmlRpcClient client = new XmlRpcClient();
		client.setConfig(config);
		
		//パラメータ設定
		Map<Object, Object> params = new HashMap<Object, Object>();
		params.put("body", text); //解析文章
		params.put("mode", "lite"); //省略するとリンクタグ付文章が返ってくる
		params.put("score", 10); //キーワードスコア
		
		try {
			@SuppressWarnings("rawtypes")
			HashMap<Object, Object> result = 
					(HashMap) client.execute("hatena.setKeywordLink", new Object[]{params});
			Object[] wordlist = (Object[]) result.get("wordlist");
			for(Object word:wordlist){
				System.out.println(word);
			}
		} catch (XmlRpcException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

}
