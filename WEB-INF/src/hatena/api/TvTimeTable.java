package hatena.api;

import hatena.db.TvInsert;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


@SuppressWarnings("deprecation")
public class TvTimeTable {

	public static void main(String[] args) {
		StringBuilder builder = new StringBuilder();

		//HTTPリクエストを送るために必要
		HttpClient client = new DefaultHttpClient();
		//GETリクエストを送るために必要
		HttpGet httpGet = new HttpGet(
				//取得したいJSON
				"http://animemap.net/api/table/tokyo.json"
				);

		try {
			//リクエストしたリンクの存在を確認
			HttpResponse response = client.execute(httpGet);
			//返ってきた値のステータスを調べる
			int statusCode = response.getStatusLine().getStatusCode();
			if(statusCode == 200){
				//レスポンスからHTTPエンティティ(実体)を生成
				HttpEntity entity = response.getEntity();
				//HTTPエンティティからコンテント(中身)を生成
				InputStream content = entity.getContent();

				//Scanner sc = new Scanner(new InputStreamReader(content));
				BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				String line;

				while((line = reader.readLine()) != null){
					builder.append(line);
				}
			}else{
				System.out.println("Failed to download File.");
			}
		} catch (ClientProtocolException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		System.out.println(builder.toString());

		//JSONオブジェクト変化
		try {
			//JSONArray生成(文字列のJSONをJSONArrayに変換)
			//JSONArray jsonArray[] = new JSONArray("[" + builder.toString() + "]");
			JSONObject root = new JSONObject(builder.toString());
			JSONObject response = root.getJSONObject("response");
			JSONArray jsonArray = response.getJSONArray("item");
			//JSONArrayのサイズを表示
			System.out.println("size:" + jsonArray.length());
			//JSONオブジェクトを作成
			for(int i = 0; i<jsonArray.length(); i++){
				JSONObject jsonObject = jsonArray.getJSONObject(i);

				String number = jsonObject.getString("url");
				int ret = Integer.parseInt(number.replaceAll("[^0-9]", ""));

				//DB Insert
				TvInsert tvInsert = new TvInsert();
				tvInsert.insert(jsonObject, ret);


				//objectをパース
				System.out.println(i);
				System.out.println("id:" + ret);
				System.out.println("タイトル: " + jsonObject.getString("title"));
				System.out.println();
			}
		} catch (JSONException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

}
