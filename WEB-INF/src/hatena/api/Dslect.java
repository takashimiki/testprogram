package hatena.api;

import hatena.db.DBOP;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Dslect extends DBOP{
	public static void main(String[] args) throws SQLException{
		Connection conn = null;
		PreparedStatement stmt;
		ResultSet resultSet =null;
		
		try {
			conn = getConnection();
			String sql = "SELECT  `title`,`title_id` FROM `tvtitle`";
			stmt = conn.prepareStatement(sql);
			logger.info(stmt.toString());
			resultSet = stmt.executeQuery();
			
			while(resultSet.next()){
				System.out.print(resultSet.getInt("title_id"));
				System.out.println(":" + resultSet.getString("title"));
			}
			
			stmt.close();
			
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} finally{
			resultSet.close();
			conn.commit();
			conn.close();
		}
	}
}
