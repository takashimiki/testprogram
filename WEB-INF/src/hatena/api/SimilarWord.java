package hatena.api;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;


//はてなキーワード連想語API
public class SimilarWord {
	 private static final String API_URL = "http://d.hatena.ne.jp/xmlrpc";
	 private static final String API_METHOD_NAME = "hatena.getSimilarWord";
	 
	 //ワードセット
	 //private static final String[] Word_List = {"のんのんびより", "アニメ"};
	 //private static final String Word_List = "進撃の巨人";
	 
	 static Scanner sc = new Scanner(System.in);
	 private static final String Word_List = sc.nextLine();

	 
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) {
		XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
		
		try {
			config.setServerURL(new URL(API_URL));
		} catch (MalformedURLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		
		XmlRpcClient client = new XmlRpcClient();
		client.setConfig(config);
		
		//パラメータ
		Map param = new HashMap();
		param.put("wordlist", Word_List);
		
		
		//APIを呼んで結果を出力
		Object result;
		try {
			result = client.execute(API_METHOD_NAME, new Object[] {param});
			if(result != null && result instanceof Map){
				Object[] wordlist =(Object[]) ((Map) result).get("wordlist");
				for(Object word : wordlist){
					System.out.println(((Map) word).get("word"));
				}
			}
		} catch (XmlRpcException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		
	}
}
