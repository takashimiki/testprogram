package hatena.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;



public class TvInsert extends DBOP {
	public int insert (JSONObject jsonObject, int ret) {
		Connection conn;
		int num = 0;

		try {
			conn = getConnection();
			String sql = "insert ignore into tvtitle (title_id, title) values (?, ?)";

			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setInt(1, ret);
			stmt.setString(2, jsonObject.getString("title"));
			num = stmt.executeUpdate();

			logger.info(sql);

			stmt.close();
			conn.commit();
			conn.close();
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return num;

	}
}
