package hatena.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TitleSelect extends DBOP{
	public List<String> select() throws SQLException{
		List<String> list = new ArrayList<String>();
		Connection conn = null;
		PreparedStatement stmt;
		ResultSet resultSet =null;

		try {
			conn = getConnection();
			String sql = "SELECT  `title` FROM `tvtitle`";
			stmt = conn.prepareStatement(sql);
			logger.info(stmt.toString());
			resultSet = stmt.executeQuery();

			while(resultSet.next()){
				list.add(resultSet.getString("title"));
			}

			stmt.close();

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}finally{
			resultSet.close();
			conn.commit();
			conn.close();
		}

		return list;

	}
}
